# A local development environment.

This is a setup for a local development environment that offers valid local web
certificates, local routing using Traefik, a PostgreSQL management container using
pgadmin, a local mail server using maildev and a whoami container.

This setup "works on my machine", which is a Manjaro (Arch based) linux, but should work
on any linux distribution and probably OSX and WSL installations as well. I've specified
the docker-compose file with a version 3.8. I have not investigated this, but I suspect
this setup works (or at least will work with some minor modifications) with any 3.*
version of compose.

If set up, you can access these containers on your dev.localhost domain:
* Traefik: [https://traefik.dev.localhost](https://traefik.dev.localhost)
* pgadmin: [https://pgadmin.dev.localhost](https://pgadmin.dev.localhost)
* maildev: [https://maildev.dev.localhost](https://maildev.dev.localhost)
* whoami: [https://whoami.dev.localhost](https://whoami.dev.localhost)

## Preparation

You will need:
* [mkcert](https://github.com/FiloSottile/mkcert) - a simple tool for making
  locally-trusted development certificates.
* [docker](https://docs.docker.com/get-docker/) - The container thingy.
* [docker-compose](https://docs.docker.com/compose/install/) - The docker container configuration app.
* [optionally] make - to use the Makefile.

## Configure the basics

If you have installed these tools, set up the local environment as follows.

### Via make
* To perform steps 2 and 3, use `make prerequisites`.
* To perform steps 1, 2 and 3, use `make prerequisites.all`

### Steps
1. Create a root certificate.
   ```bash
   mkcert -install
   ```
2. Create a wildcard certificate for the `dev.localhost` domain. Then move the certificates
   into the `traefik/certs` folder.
   ```bash
   mkcert "*.dev.localhost"
   mv _wildcard.dev.localhost*.pem ./traefik/certs/
   ```
   *Please note that these wildcard certificates only support a single subdomain. If
   you need sub-subdomains, you need to add certificates for that via
   `mkcert foo.bar.dev.localhost`
3. Create a docker network and a data volume:
   ```bash
   docker volume create dev-local-data-pgadmin
   docker network create dev-local-network
   ```
   If you redo this step, and either the docker volume or the docker network already
   exists, you can remove them (irrevokably) as follows:
   ```bash
   docker volume rm dev-local-data-pgadmin
   # and/or
   docker network rm dev-local-network
   ```
    Or, if you prefer to use make, remove them both in one go with
   `make docker.clean`.

#### Clearing the dns cache.
The dnsmasq service will clear its cache on reloading. To reload:
```bash
docker-compose exec -d dns pkill -HUP dnsmasq
# or, using make
make dns.reload.
```

## pgadmin credentials
Before you start up the pgadmin container, you must copy the `pgadmin/.example.env` file
to `pgadmin/.env`. I suggest you alter the values in there. The email address must be
valid, it cannot be @dev.localhost or @example.com. Just to be sure, use a random password
and something you never used before.

## Startup

Run `docker-compose up -d` to start the services.

The containers are configured to automatically start, unless explicitly stoped.
