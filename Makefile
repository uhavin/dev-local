prerequisites.root_cert:
	mkcert -install
prerequisites.dev_cert:
	mkcert "*.dev.localhost"
	mv _wildcard.dev.localhost.pem ./traefik/certs/
	mv _wildcard.dev.localhost-key.pem ./traefik/certs/
prerequisites.docker:
	docker volume create dev-local-data-pgadmin
	docker network create dev-local-network


prerequisites: prerequisites.dev_cert prerequisites.docker
prerequisites.all: prerequisites.docker prerequisites.root_cert prerequisites.dev_cert

docker.clean:
	docker volume rm dev-local-data-pgadmin
	docker network rm dev-local-network

run:
	docker-compose up -d
update:
	docker-compose pull
